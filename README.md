# django_intro_2022_09_13_gr_1406

---

[video](https://youtu.be/7ZmJRre7MYg)

[video 2 - routing - 2022-09-15](https://youtu.be/dg749YY6mWw)

[video 3 models](https://youtu.be/6EsSM8_Cj9w)

[video 4 templates views](https://youtu.be/EiVIvGhnKTg)

[video 5 - part 1 forms](https://youtu.be/lRMQSURjWrw)

[video 6 - part 2 forms](https://youtu.be/x9UPy2Y-1lA)

[video 7 - DRF](https://youtu.be/gHBaAqvfY1E)



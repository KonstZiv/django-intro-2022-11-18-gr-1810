from django.http import HttpResponse, HttpRequest


def index(request: HttpRequest) -> HttpResponse:
    return  HttpResponse("it is start page")


def special_case_2003(request: HttpRequest) -> HttpResponse:
    return HttpResponse("special case 2003")


def year_archive(request: HttpRequest, year) -> HttpResponse:
    return HttpResponse(f"year archive - {year}")


def month_archive(request: HttpRequest, year, month) -> HttpResponse:
    return HttpResponse(f"month archive - year: {year}, month: {month}")


def article_detail(request: HttpRequest, year, month, slug) -> HttpResponse:
    return HttpResponse(f"article detail:\nyear: {year}\nmonth: {month}\nslug: {slug}")
